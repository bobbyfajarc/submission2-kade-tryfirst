package com.bobbyfajarc.footballmatchschedule.main

import com.bobbyfajarc.footballmatchschedule.CoroutineContextProvider
import com.bobbyfajarc.footballmatchschedule.api.ApiRepository
import com.bobbyfajarc.footballmatchschedule.api.TheSportDBApi
import com.bobbyfajarc.footballmatchschedule.model.TeamResponse
import com.bobbyfajarc.footballmatchschedule.model.MainResponse
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class DetailPresenter(private val view: DetailView,
                      private val apiRepository: ApiRepository,
                      private val gson: Gson,
                      private val context: CoroutineContextProvider = CoroutineContextProvider()
) {

    fun getDetailEvent(leagueId: String?) {
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getDetailMatch(leagueId)), MainResponse::class.java)
            }
            view.showDetailEvent(data.await().mains)
        }
    }

    fun getDetailHomeTeam(teamId: String?) {
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getDetailTeam(teamId)), TeamResponse::class.java)
            }
            view.showDetailHomeTeam(data.await().teams)
            view.hideLoading()
        }
    }

    fun getDetailAwayTeam(teamId: String?) {
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getDetailTeam(teamId)), TeamResponse::class.java)
            }
            view.showDetailAwayTeam(data.await().teams)
            view.hideLoading()
        }
    }
}