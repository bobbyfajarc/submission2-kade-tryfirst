package com.bobbyfajarc.footballmatchschedule.main

import com.bobbyfajarc.footballmatchschedule.model.Main

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showEventList(data: List<Main>)
}