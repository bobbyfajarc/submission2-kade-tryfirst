package com.bobbyfajarc.footballmatchschedule.main

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.bobbyfajarc.footballmatchschedule.R
import com.bobbyfajarc.footballmatchschedule.R.color.colorAccent
import com.bobbyfajarc.footballmatchschedule.Utils.invisible
import com.bobbyfajarc.footballmatchschedule.Utils.visible
import com.bobbyfajarc.footballmatchschedule.api.ApiRepository
import com.bobbyfajarc.footballmatchschedule.api.Constants
import com.bobbyfajarc.footballmatchschedule.model.Main
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class NextMatchFragment : Fragment(), AnkoComponent<Context>, MainView {

    private var mains: MutableList<Main> = mutableListOf()
    private lateinit var presenter: MainPresenter
    private lateinit var adapter: MainAdapter
    private lateinit var listEvent: RecyclerView

    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        showLoading()

        adapter = MainAdapter(mains) {
            ctx.startActivity<DetailActivity>("eventId" to "${it.eventId}")
        }
        listEvent.adapter = adapter

        val request = ApiRepository()
        val gson = Gson()
        presenter = MainPresenter(this, request, gson)
        presenter.getNextMatch(Constants.LEAGUE_ID_PREMIER_LEAGUE)

        swipeRefreshLayout.onRefresh {
            presenter.getNextMatch(Constants.LEAGUE_ID_PREMIER_LEAGUE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            orientation = LinearLayout.VERTICAL
            backgroundColor = Color.WHITE
            topPadding = dip(16)
            leftPadding = dip(16)
            rightPadding = dip(16)

            swipeRefreshLayout = swipeRefreshLayout {
                setColorSchemeResources(
                    colorAccent,
                    android.R.color.holo_blue_bright,
                    android.R.color.holo_orange_dark)

                relativeLayout{
                    lparams (width = matchParent, height = wrapContent)

                    listEvent = recyclerView {
                        id = R.id.list_event
                        lparams (width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                    }.lparams{
                        centerHorizontally()
                    }
                }
            }
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showEventList(data: List<Main>) {
        swipeRefreshLayout.isRefreshing = false
        mains.clear()
        mains.addAll(data)
        adapter.notifyDataSetChanged()
    }

}

