package com.bobbyfajarc.footballmatchschedule.main

import com.bobbyfajarc.footballmatchschedule.CoroutineContextProvider
import com.bobbyfajarc.footballmatchschedule.api.ApiRepository
import com.bobbyfajarc.footballmatchschedule.api.TheSportDBApi
import com.bobbyfajarc.footballmatchschedule.model.MainResponse
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class MainPresenter(private val view: MainView,
                    private val apiRepository: ApiRepository,
                    private val gson: Gson,
                    private val context: CoroutineContextProvider = CoroutineContextProvider()
) {

    fun getLastMatch(leagueId: String?) {
        async(context.main){
            val data = bg {
                gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getLastMatch(leagueId)),
                    MainResponse::class.java
                )
            }
            view.showEventList(data.await().mains)
            view.hideLoading()
        }
    }

    fun getNextMatch(leagueId: String?) {
        async(context.main){
            val data = bg {
                gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getNextMatch(leagueId)),
                    MainResponse::class.java
                )
            }
            view.showEventList(data.await().mains)
            view.hideLoading()
        }
    }


}