package com.bobbyfajarc.footballmatchschedule.main

import com.bobbyfajarc.footballmatchschedule.model.Team
import com.bobbyfajarc.footballmatchschedule.model.Main

interface DetailView {
    fun initView()
    fun showLoading()
    fun hideLoading()
    fun showDetailEvent(data: List<Main>)
    fun showDetailHomeTeam(data: List<Team>)
    fun showDetailAwayTeam(data: List<Team>)
}