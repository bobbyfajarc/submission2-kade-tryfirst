package com.bobbyfajarc.footballmatchschedule.model

data class TeamResponse(
    val teams: List<Team>)