package com.bobbyfajarc.footballmatchschedule.model

data class MainResponse(
    val mains: List<Main>)